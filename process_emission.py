import json
import logging
import sys
import platform

from threading import Timer
import greengrasssdk

# Logging
logger = logging.getLogger(__name__)
logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)

# SDK Client
client = greengrasssdk.client("iot-data")

# Counter
max_co2_vals = [0] * 5


def lambda_handler(event, context):
    global max_co2_vals

    # Get your data
    co2_val, time_stamp, vehicle = event['message'], event['time_stamp'], event['vehicle']

    # Update max CO2 value
    vehicle_index = int(vehicle)
    max_co2_vals[vehicle_index] = max(co2_val, max_co2_vals[vehicle_index])

    # Publish result
    client.publish(
        topic="iot/vehicle",
        queueFullPolicy="AllOrException",
        payload=json.dumps({
            "co2": co2_val,
            "max_co2": max_co2_vals[vehicle_index],
            "time_stamp": time_stamp,
            "vehicle": vehicle
        }),
    )

    return
